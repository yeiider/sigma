import React from 'react';
import Header from './component/header'
import Main from './component/main'
import '../node_modules/font-awesome/css/font-awesome.css'
import './index.css'

import '../node_modules/bootstrap/dist/css/bootstrap.css'

function App() {
  return (
    <div className="container mt-5">
      <Header/>
      <Main/>
    </div>
    
  );
}

export default App;
