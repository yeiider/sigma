import React, { Component } from 'react'
import imagen from '../sigma-image.png'
import Modal from './modal'

export default class Main extends Component{
    
    constructor(props){
        super(props)
        this.state={
            data:{},
            departamentos:[],
            ciudades:[],
            modalOpen:false,
            modalClose:false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeModal = this.closeModal.bind(this);
		this.openModal = this.openModal.bind(this);
    }
   async componentDidMount(){
        try {
            let params={
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },
                method:'GET'
            }
            let proxyUrl='https://cors-anywhere.herokuapp.com/',
                targetUrl='https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json'
            let request = await fetch(proxyUrl + targetUrl,{params})
            let data = await request.json();
            this.setState({
                data:data,
                departamentos:Object.keys(data),
            })
           if(window.location.search){
               this.openModal()
           }
        } catch (error) {
            console.log(error)
        }      
    }
    openModal() {
		this.setState({
			modalOpen: true
        })
    }

     closeModal() {
		this.setState({
			modalOpen: false
         });
	}
    handleSubmit(e){
        e.preventDefault()
        if(this.validateForm()){
            this.openModal()
            this.form.submit()
        }
       
    }

    resetField(){
        this.nombre.value=""
        this.email.value=""
    }
    departamentoChange = (e) =>{
       let key_departamento = e.target.value
       this.setState({
           ciudades:this.state.data[key_departamento]
       })    
      
    }
    

    async sendForm(){
     /* const data = {
          "email":this.email.value,
          "name":this.nombre.value,
          "city":this.departamento.value,
          "state":this.ciudad.vale,
      }*/
     const data = new FormData()
          data.append('email',`${this.email.value}`)
          data.append('name',`${this.nombre.value}`)
          data.append('city',`${this.departamento.value}`)
          data.append('state',`${this.ciudad.value}`)
        var requestOptions = {
        method: 'POST',
        headers: {'Content-Type':'application/json'},
        body: data,
        mode:'no-cors'
        
        };

      try {
        let request = await fetch('http://localhost:3380/api/v1/contacto',requestOptions)
        let response = await request.json()
        this.openModal()
      }catch (error) {
         
      }     
    }
    validateForm = (e)=>{

     let validaEmail=/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i
     let errors=[];
     if(!this.email.value){
        errors.push("El campo email es requerido")
     }
     if(!this.nombre.value){
         errors.push("El campo nombre es requerido")
     }
     if(!this.departamento.value){
         errors.push("Debe seleccionar un departamento")
     }
     if(!this.ciudad.value){
         errors.push("Debe seleccionar una ciudad")
     }
     if(!validaEmail.test(this.email.value)){
         errors.push("Email invalido")
     }

     if(errors.length>0){
        let errorMessage = ""
        errors.forEach(e =>{
            errorMessage+=`<li>${e}</li>`
        })
        let printError=`<div class='alert alert-danger' role='alert' ><ul>${errorMessage}</ul></div>`
        document.getElementById('alert').innerHTML=printError
     }else{
         
         return true
     }
     return false
    }    
      
    
  render(){
      const styles={
          img_main:{
            width:'100%'
          },
          btn_color:{
                backgroundColor: '#e14765',
                color:'white',
                borderRadius: '30px',
                width:'40%'                
              }
          }
    
      return(
          <div className="row col-10 m-auto  container">
            <div className="col-xs-12 col-sm-12 mt-5 col-lg-6">
                <img src={imagen} className="ima-main" alt="imagen" style={styles.img_main}></img>
            </div>       
            <div className="col-xs-12 col-sm-12 col-lg-6 form pt-5 pb-3">
                <form action="http://localhost:3380/api/v1/contacto" method="POST" ref={form => this.form = form} >
                    <div className="" id="alert">

                    </div>
                    <div className="form-group">
                        <label form="departamento">Departamento*</label>
                       <select className="form-control"  onChange={this.departamentoChange} id="departamento" name="state"   ref={select => this.departamento = select}>
                           <option value="">Seleccione...</option>
                           
                         { this.state.departamentos.map((d,k)=>(
                             <option key={k} value={d}>{d}</option>
                         ))}
                       </select>
                    </div>
                    <div className="form-group">
                        <label form="ciudad">Ciudad*</label>
                        <select className="form-control" id="ciudad"  name="city"   ref={select => this.ciudad = select}>
                        
                           { this.state.ciudades.map((c,k)=>(
                               <option key={k} value={c}>{c}</option>
                           ))}
                       </select>
                    </div>
                    <div className="form-group">
                        <label form="nombre">Nombre*</label>
                        <input type="text" className="form-control" name="name" id="nombre" placeholder="Pepito de Jesus" ref={input => this.nombre = input}></input>
                    </div>
                    <div className="form-group">
                        <label form="email">Correo*</label>
                        <input type="email"  className="form-control" name="email"  id="email" placeholder="pepitodejesus@mail.com"   ref={input => this.email = input}></input>
                    </div>
                    <div className="form-group text-center">
                      <button type="submit" className="btn btn-color pl-3 btn-lg " style={styles.btn_color} onClick={this.handleSubmit}>Enviar</button>
                    </div>
                </form>
               
            </div>
            <Modal
            isModalOpen={this.state.modalOpen}
            closeModal={this.closeModal}/>
          </div>
      )
  }
}