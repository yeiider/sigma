import React from 'react'
import logo from '../logo.png'
const Header = ()=>{
    return (
        <div className="mb-5">
            <div className="nav">
            <img src={logo} className="rounded mx-auto d-block" alt="logo_sigma"/>
            </div>
            <div className="text-center mt-4">
               <h2> 
                   Prueba de desarrollo Sigma
               </h2>

               <p className="text-head">Do enim excepteur do veniam ex duis esse elit duis aliqua sit est amet ipsum. Adipisicing mollit cillum qui ipsum aute id dolore est velit et dolore. Quis deserunt eiusmod pariatur proident dolore in ullamco id laboris</p>
            </div>
        </div>
    )
}

export default Header