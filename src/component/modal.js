import React from 'react'
import PropTypes from 'prop-types';



export default class Modal extends React.Component {  
	static propTypes = {
		isModalOpen: PropTypes.bool.isRequired,
		closeModal: PropTypes.func.isRequired
    };

	render() {
		return (

    <div className="modal" id="staticBackdrop" style={{display: this.props.isModalOpen ? 'block' : 'none'}}>
  <div className="modal-dialog">
    <div className="modal-content">
      <div className="modal-body">
        <div className="alert alert-info p-5" role="alert">
        Tu información ha sido recibida satisfactoriamente
        </div>
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary"  onClick={this.props.closeModal} data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>


		);
	}
}

