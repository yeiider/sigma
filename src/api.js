const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const rutasbackend= require('./backend/rutas')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(rutasbackend)

app.listen(3380, () => {
  console.log("Servidor iniciado")
})